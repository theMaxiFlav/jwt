package com.example.jwt.rest;

import com.example.jwt.domain.HttpResponse;
import com.example.jwt.domain.User;
import com.example.jwt.domain.UserPrincipal;
import com.example.jwt.exception.domain.*;
import com.example.jwt.service.UserService;
import com.example.jwt.utility.JWTTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import static com.example.jwt.constant.fileconstant.FileConstant.FORWARD_SLASH;
import static com.example.jwt.constant.fileconstant.FileConstant.USER_FOLDER;
import static com.example.jwt.constant.securityconstant.SecurityConstant.JWT_TOKEN_HEADER;

@RestController
@RequestMapping(path = {"/", "/user"})
public class UserRestController extends ExceptionHandling {

    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final JWTTokenProvider jwtTokenProvider;

    @Autowired
    public UserRestController(UserService userService, AuthenticationManager authenticationManager, JWTTokenProvider jwtTokenProvider) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @PostMapping(value = "/login")
    public ResponseEntity<User> login(@RequestBody User user) {
        authenticate(user.getUsername(), user.getPassword());
        User loginUser = userService.findUserByUsername(user.getUsername());
        UserPrincipal userPrincipal = new UserPrincipal(loginUser);
        HttpHeaders jwtHeader = getJwtHeader(userPrincipal);
        return new ResponseEntity<>(loginUser, jwtHeader, HttpStatus.OK);
    }

    private HttpHeaders getJwtHeader(UserPrincipal userPrincipal) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(JWT_TOKEN_HEADER, jwtTokenProvider.getJwtToken(userPrincipal));
        return headers;
    }

    private void authenticate(String username, String password) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
    }

    @PostMapping(value = "/register")
    public ResponseEntity<User> registerUser(@RequestBody User user) throws UsernameAlreadyExistsException, EmailAlreadyExistsException, MessagingException, EmailPatternException {
        User registerUser = userService.register(user.getFirstName(), user.getLastName(), user.getUsername(), user.getEmail());
        return new ResponseEntity<>(registerUser, HttpStatus.OK);
    }

    @PostMapping("/add")
    @PreAuthorize("hasAnyAuthority('user:create')")
    public ResponseEntity<User> addUser(@RequestParam("firstName") String firstName,
                                        @RequestParam("lastName") String lastName,
                                        @RequestParam("username") String username,
                                        @RequestParam("email") String email,
                                        @RequestParam("role") String role,
                                        @RequestParam("isActive") boolean isActive,
                                        @RequestParam("isNotLocked") boolean isNotLocked,
                                        @RequestParam(value = "profileImage", required = false) MultipartFile profileImage) throws UsernameAlreadyExistsException, EmailAlreadyExistsException, IOException, EmailPatternException {
        User newUser = userService.addUser(firstName, lastName, username, email, role, isNotLocked, isActive, profileImage);
        return new ResponseEntity<>(newUser, HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority('user:update')")
    @PatchMapping("/update")
    public ResponseEntity<User> updateUser(@RequestParam("currentUsername") String currentUsername,
                                        @RequestParam("firstName") String firstName,
                                        @RequestParam("lastName") String lastName,
                                        @RequestParam("username") String username,
                                        @RequestParam("email") String email,
                                        @RequestParam("role") String role,
                                        @RequestParam("isActive") boolean isActive,
                                        @RequestParam("isNotLocked") boolean isNotLocked,
                                        @RequestParam(value = "profileImage", required = false) MultipartFile profileImage) throws UsernameAlreadyExistsException, EmailAlreadyExistsException, IOException, EmailPatternException {
        User updatedUser = userService.updateUser(currentUsername, firstName, lastName, username, email, role, isNotLocked, isActive, profileImage);
        return new ResponseEntity<>(updatedUser, HttpStatus.OK);
    }

    @GetMapping("/{username}")
    public ResponseEntity<User> getUser(@PathVariable("username") String username) {
        User user = userService.findUserByUsername(username);
        return new ResponseEntity<>(user, HttpStatus.FOUND);
    }

    @GetMapping("/list")
    public ResponseEntity<List<User>> getAllUsers(@PathVariable("username") String username) {
        List<User> users = userService.getUsers();
        return new ResponseEntity<>(users, HttpStatus.FOUND);
    }

    @PreAuthorize("hasAnyAuthority('user:update')")
    @PatchMapping("/update-image-profile")
    public ResponseEntity<User> updateProfileImage(@RequestParam("username") String username,
                                           @RequestParam("profileImage") MultipartFile profileImage) throws UsernameAlreadyExistsException, EmailAlreadyExistsException, IOException {
        User user = userService.updateProfileImage(username, profileImage);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping("/resetPassword/{email}")
    public ResponseEntity<HttpResponse> resetPassword(@PathVariable("email") String email) throws EmailNotFoundException {
        userService.resetPassword(email);
        return response(HttpStatus.OK, "Here your new password you little silly !");
    }

    @PreAuthorize("hasAnyAuthority('user:delete')")
    @DeleteMapping("/remove-user/{id}")
    public ResponseEntity<HttpResponse> deleteUser(@PathVariable("id") long id) {
        userService.deleteUser(id);
        return response(HttpStatus.OK, String.format("User with id %s has been removed", id));
    }

    @GetMapping(path = "/image/{username}/{fileName}", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getProfileImage(@PathVariable("username") String username, @PathVariable("fileName") String fileName) throws IOException {
        return Files.readAllBytes(Paths.get(USER_FOLDER + username + FORWARD_SLASH + fileName));
    }

    private ResponseEntity<HttpResponse> response(HttpStatus status, String message) {
        return new ResponseEntity<>(new HttpResponse(status.value(), status, status.getReasonPhrase(), message.toUpperCase(), new Date()), status);
    }
}
