package com.example.jwt.service.impl;

import com.example.jwt.domain.User;
import com.example.jwt.domain.UserPrincipal;
import com.example.jwt.enumeration.Role;
import com.example.jwt.exception.domain.EmailAlreadyExistsException;
import com.example.jwt.exception.domain.EmailNotFoundException;
import com.example.jwt.exception.domain.EmailPatternException;
import com.example.jwt.exception.domain.UsernameAlreadyExistsException;
import com.example.jwt.repository.UserRepository;
import com.example.jwt.service.EmailService;
import com.example.jwt.service.LoginAttemptService;
import com.example.jwt.service.UserService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import static com.example.jwt.constant.fileconstant.FileConstant.*;
import static com.example.jwt.constant.userconstant.UserConstant.*;
import static com.example.jwt.enumeration.Role.ROLE_USER;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Service
@Transactional
@Qualifier("userDetailsService")
public class UserServiceImpl implements UserService, UserDetailsService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final LoginAttemptService loginAttemptService;
    private final EmailService emailService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder, LoginAttemptService loginAttemptService,
                           EmailService emailService) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.loginAttemptService = loginAttemptService;
        this.emailService = emailService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findUserByUsername(username);
        if(user == null) {
            LOGGER.error(NO_USER_FOUND_BY_USERNAME + username);
            throw new UsernameNotFoundException(NO_USER_FOUND_BY_USERNAME + username);
        } else {
            validateLoginAttempts(user);
            user.setLastLoginDateDisplay(user.getLastLoginDate());
            user.setLastLoginDate(new Date());
            userRepository.save(user);
            UserPrincipal userPrincipal = new UserPrincipal(user);
            LOGGER.info("User founded by username : " + username);

            return userPrincipal;
        }
    }

    private void validateLoginAttempts(User user) {
        if(user.isNotLocked()) {
            if(loginAttemptService.hasExceededMaxAttempts(user.getUsername())) {
                user.setNotLocked(false);
            } else {
                user.setNotLocked(true);
            }
        } else {
            loginAttemptService.evictUserFromLoginAttemptCache(user.getUsername());
        }
    }

    @Override
    public User register(String firstName, String lastName, String username, String email) throws UsernameAlreadyExistsException, EmailAlreadyExistsException, EmailPatternException {
        validateEmailPattern(email);
        validateNewUsernameAndEmail(StringUtils.EMPTY, username, email);
        User user = new User();
        user.setUserId(generateUserId());
        String password = generatePassword();
        String encodedPassword = encodeUserPassword(password);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setUsername(username);
        user.setEmail(email);
        user.setJoinedDate(new Date());
        user.setPassword(encodedPassword);
        user.setActive(true);
        user.setNotLocked(true);
        user.setRoles(ROLE_USER.name());
        user.setAuthorities(ROLE_USER.getAuthorities());
        user.setProfileImageUrl(getTempProfileImageUrl(username));
        userRepository.save(user);
       // emailService.sendNewPasswordEmail(firstName, password, email);

        LOGGER.info(String.format("User [%s] has been created", username));
        return user;
    }

    //TODO: refresh this method
    private User validateNewUsernameAndEmail(String currentUsername, String newUsername, String newEmail) throws UsernameAlreadyExistsException, EmailAlreadyExistsException {
        User userByUsername = findUserByUsername(newUsername);
        User userByNewEmail = findUserByEmail(newEmail);
        if(StringUtils.isNotBlank(currentUsername)) {
            User currentUser = findUserByUsername(currentUsername);
            if(currentUser == null) {
                LOGGER.error(NO_USER_FOUND_BY_USERNAME + currentUsername);
                throw new UsernameNotFoundException(NO_USER_FOUND_BY_USERNAME + currentUsername);
            }

            if(userByUsername != null && !currentUser.getId().equals(userByUsername.getId())) {
                LOGGER.error(String.format(USERNAME_ALREADY_EXISTS, userByUsername.getUsername()));
                throw new UsernameAlreadyExistsException(String.format(USERNAME_ALREADY_EXISTS, userByUsername.getUsername()));
            }

            if(userByNewEmail != null && !currentUser.getId().equals(userByNewEmail.getId())) {
                LOGGER.error(String.format(EMAIL_ALREADY_EXISTS, userByNewEmail.getEmail()));
                throw new EmailAlreadyExistsException(String.format(EMAIL_ALREADY_EXISTS, userByNewEmail.getEmail()));
            }
            return currentUser;
        } else {

            if(userByUsername != null) {
                throw new UsernameAlreadyExistsException(String.format(USERNAME_ALREADY_EXISTS, userByUsername.getUsername()));
            }
            if(userByNewEmail != null) {
                throw new EmailAlreadyExistsException(String.format(EMAIL_ALREADY_EXISTS, userByNewEmail.getEmail()));
            }
            return null;
        }
    }

    private void validateEmailPattern(String email) throws EmailPatternException {
        String regexPattern = "^(.+)@(\\S+)$";
        if (!Pattern.matches(regexPattern, email)) {
            throw new EmailPatternException("Email not valid");
        }
    }

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public User findUserByUsername(String username) {
        return userRepository.findUserByUsername(username);
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    @Override
    public User addUser(String firstName, String lastName, String username, String email, String role, boolean isNotLocked, boolean isActive, MultipartFile profileImage) throws UsernameAlreadyExistsException, EmailAlreadyExistsException, EmailPatternException, IOException {
        validateNewUsernameAndEmail(StringUtils.EMPTY, username, email);
        validateEmailPattern(email);
        User user = new User();
        user.setUserId(generateUserId());
        String password = generatePassword();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setJoinedDate(new Date());
        user.setUsername(username);
        user.setEmail(email);
        user.setPassword(encodeUserPassword(password));
        user.setActive(isActive);
        user.setNotLocked(isNotLocked);
        user.setAuthorities(getRoleEnum(role).getAuthorities());
        user.setRoles(getRoleEnum(role).name());
        user.setProfileImageUrl(getTempProfileImageUrl(username));
        userRepository.save(user);
        saveProfileImage(user, profileImage);
        LOGGER.info(String.format("User %s has been successfully created", username));
        return null;
    }

    private void saveProfileImage(User user, MultipartFile profileImage) throws IOException {
        if(profileImage != null) {
            Path userFolder = Paths.get(USER_FOLDER + user.getUsername()).toAbsolutePath().normalize();
            if(!Files.exists(userFolder)) {
                Files.createDirectories(userFolder);
                LOGGER.info(String.format(DIRECTORY_CREATED, userFolder.getFileName()));
            }
            Files.deleteIfExists(Paths.get(userFolder + user.getUsername() + DOT + JPG_EXTENSION));
            Files.copy(profileImage.getInputStream(), userFolder.resolve(user.getUsername() + DOT + JPG_EXTENSION), REPLACE_EXISTING);
            user.setProfileImageUrl(setProfileImageUrl(user.getUsername()));

            userRepository.save(user);
            LOGGER.info(String.format(FILE_SAVED_IN_FILE_SYSTEM, profileImage.getName()));
        }
    }

    private String setProfileImageUrl(String username) {
        return ServletUriComponentsBuilder.fromCurrentContextPath().path(USER_IMAGE_PATH + username + FORWARD_SLASH + username + DOT + JPG_EXTENSION).toUriString();
    }

    private Role getRoleEnum(String role) {
        return Role.valueOf(role.toUpperCase());
    }
    private String getTempProfileImageUrl(String username) {
        return ServletUriComponentsBuilder.fromCurrentContextPath().path(DEFAULT_USER_IMAGE_PATH + username).toUriString();
    }

    private String encodeUserPassword(String password) {
        return bCryptPasswordEncoder.encode(password);
    }

    private String generatePassword() {
        return RandomStringUtils.randomAlphabetic(10);
    }

    private String generateUserId() {
        return RandomStringUtils.randomNumeric(10);
    }


    @Override
    public User updateUser(String currentUsername, String newFirstName, String newLastName, String newUsername, String newEmail, String role, boolean isNotLocked, boolean isActive, MultipartFile profileImage) throws UsernameAlreadyExistsException, EmailAlreadyExistsException, EmailPatternException, IOException {
        User currentUser = validateNewUsernameAndEmail(currentUsername, newUsername, newEmail);
        validateEmailPattern(newEmail);

        currentUser.setJoinedDate(new Date());
        currentUser.setFirstName(newFirstName);
        currentUser.setLastName(newLastName);
        currentUser.setUsername(newUsername);
        currentUser.setEmail(newEmail);
        currentUser.setActive(isActive);
        currentUser.setNotLocked(isNotLocked);
        currentUser.setRoles(getRoleEnum(role).name());
        currentUser.setAuthorities(getRoleEnum(role).getAuthorities());
        userRepository.save(currentUser);
        saveProfileImage(currentUser, profileImage);
        LOGGER.info(String.format("User %s has been successfully updated", newUsername));
        return null;
    }

    @Override
    public void deleteUser(long id) {
        userRepository.deleteById(id);
        LOGGER.info(String.format("User with id [%s] has been successfully removed", id));
    }

    @Override
    public void resetPassword(String email) throws EmailNotFoundException {
        User user = userRepository.findUserByEmail(email);
        if (user == null) {
            throw new EmailNotFoundException(String.format(NO_EMAIL_FOUND, email));
        }
        String password = generatePassword();
        user.setPassword(encodeUserPassword(password));
        userRepository.save(user);

    }

    @Override
    public User updateProfileImage(String username, MultipartFile profileImage) throws UsernameAlreadyExistsException, EmailAlreadyExistsException, IOException {
        User user = validateNewUsernameAndEmail(username, null, null);
        saveProfileImage(user, profileImage);
        LOGGER.info(String.format("User %s has updated his profile image", username));
        return user;
    }
}
