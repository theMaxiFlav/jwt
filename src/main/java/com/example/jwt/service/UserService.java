package com.example.jwt.service;

import com.example.jwt.domain.User;
import com.example.jwt.exception.domain.EmailAlreadyExistsException;
import com.example.jwt.exception.domain.EmailNotFoundException;
import com.example.jwt.exception.domain.EmailPatternException;
import com.example.jwt.exception.domain.UsernameAlreadyExistsException;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

public interface UserService {

    User register(String firstName, String lastName, String username, String email) throws UsernameAlreadyExistsException, EmailAlreadyExistsException, MessagingException, EmailPatternException;

    List<User> getUsers();

    User findUserByUsername(String username);

    User findUserByEmail(String email);

    User addUser(String firstName, String lastName, String username, String email, String role, boolean isNotLocked, boolean isActive, MultipartFile profileImage) throws UsernameAlreadyExistsException, EmailAlreadyExistsException, EmailPatternException, IOException;

    User updateUser(String currentUsername, String newFirstName, String newLastName, String newUsername, String newEmail, String role, boolean isNotLocked, boolean isActive, MultipartFile profileImage) throws UsernameAlreadyExistsException, EmailAlreadyExistsException, EmailPatternException, IOException;

    void deleteUser(long id);

    void resetPassword(String email) throws EmailNotFoundException;

    User updateProfileImage(String username, MultipartFile profileImage) throws UsernameAlreadyExistsException, EmailAlreadyExistsException, IOException;
}

