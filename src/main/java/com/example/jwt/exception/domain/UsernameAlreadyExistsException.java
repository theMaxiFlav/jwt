package com.example.jwt.exception.domain;

public class UsernameAlreadyExistsException extends Exception{

    public UsernameAlreadyExistsException(String message) {
        super(message);
    }
}
