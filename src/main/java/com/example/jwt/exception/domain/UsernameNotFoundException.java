package com.example.jwt.exception.domain;

public class UsernameNotFoundException extends Exception{

    public UsernameNotFoundException(String message) {
        super(message);
    }
}
