package com.example.jwt.exception.domain;

public class EmailPatternException extends Exception {

    public EmailPatternException(String message) {
        super(message);
    }
}
