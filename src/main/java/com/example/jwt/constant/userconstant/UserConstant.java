package com.example.jwt.constant.userconstant;

public class UserConstant {

    public static final String USERNAME_ALREADY_EXISTS = "Username [%s] already exists";
    public static final String NO_USER_FOUND_BY_USERNAME = "No user found by username ";
    public static final String NO_EMAIL_FOUND = "No email found by email %s";
    public static final String EMAIL_ALREADY_EXISTS = "Email [%s] already exists ";

}
