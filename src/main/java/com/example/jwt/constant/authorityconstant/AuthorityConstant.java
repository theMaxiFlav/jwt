package com.example.jwt.constant.authorityconstant;

public class AuthorityConstant {

    public static final String[] USER_AUTHORITIES = {"user:read"};
    public static final String[] HUMAN_RESOURCE_AUTHORITIES = {"user:read", "user:update"};
    public static final String[] MANAGER_AUTHORITIES = {"user:read", "user:update"};
    public static final String[] ADMIN_AUTHORITIES = {"user:read", "user:update", "user:create"};
    public static final String[] SUPER_ADMIN_AUTHORITIES = {"user:read", "user:update", "user:create", "user:delete"};
}
