package com.example.jwt.constant.fileconstant;

public class FileConstant {

    public static final String USER_IMAGE_PATH = "/user/image";
    public static final String JPG_EXTENSION = "jpg";
    public static final String USER_FOLDER = System.getProperty("user.home") + "/JWT/user/";
    public static final String DIRECTORY_CREATED = "Created directory for: %s  ";
    public static final String FILE_SAVED_IN_FILE_SYSTEM = "Saved file in file system %s: ";
    public static final String DOT = ".";
    public static final String FORWARD_SLASH = "/";
    public static final String TEMP_PROFILE_IMAGE_BASE_URL = "https://robohash.org/";
    public static final String DEFAULT_USER_IMAGE_PATH = "/user/image/profile/";

}
