package com.example.jwt.enumeration;

import com.example.jwt.constant.authorityconstant.AuthorityConstant;

public enum Role {

    ROLE_USER(AuthorityConstant.USER_AUTHORITIES),
    ROLE_HUMAN_RESOURCE(AuthorityConstant.HUMAN_RESOURCE_AUTHORITIES),
    ROLE_MANAGER(AuthorityConstant.MANAGER_AUTHORITIES),
    ROLE_ADMIN(AuthorityConstant.ADMIN_AUTHORITIES),
    ROLE_SUPER_ADMIN(AuthorityConstant.SUPER_ADMIN_AUTHORITIES);

    private String[] authorities;

    Role(String ... authorities) {
        this.authorities = authorities;
    }

    public String[] getAuthorities() {
        return authorities;
    }
}
